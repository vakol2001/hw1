﻿using Newtonsoft.Json;
using ProjectManager.BL.Entities;
using ProjectManager.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProjectManager.BL.Services
{
    public class ConnectService : IConnectService
    {
        private readonly HttpClient _client;

        public ConnectService(string connectionString = "https://bsa21.azurewebsites.net/api/")
        {
            _client = new HttpClient
            {
                BaseAddress = new Uri(connectionString)
            };
        }

        private bool _disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            if (disposing)
            {
                _client?.Dispose();
            }
            _disposed = true;
        }

        public async Task<Project> GetProjectAsync(int id)
        {
            return JsonConvert.DeserializeObject<Project>(await _client.GetStringAsync($"projects/{id}"));
        }

        public async Task<IEnumerable<Project>> GetProjectsAsync()
        {
            return JsonConvert.DeserializeObject<IEnumerable<Project>>(await _client.GetStringAsync($"projects"));
        }

        public async Task<Entities.Task> GetTaskAsync(int id)
        {
            return JsonConvert.DeserializeObject<Entities.Task>(await _client.GetStringAsync($"tasks/{id}"));
        }

        public async Task<IEnumerable<Entities.Task>> GetTasksAsync()
        {
            return JsonConvert.DeserializeObject<IEnumerable<Entities.Task>>(await _client.GetStringAsync($"tasks"));
        }

        public async Task<Team> GetTeamAsync(int id)
        {
            return JsonConvert.DeserializeObject<Team>(await _client.GetStringAsync($"teams/{id}"));
        }

        public async Task<IEnumerable<Team>> GetTeamsAsync()
        {
            return JsonConvert.DeserializeObject<IEnumerable<Team>>(await _client.GetStringAsync($"teams"));
        }

        public async Task<User> GetUserAsync(int id)
        {
            return JsonConvert.DeserializeObject<User>(await _client.GetStringAsync($"users/{id}"));
        }

        public async Task<IEnumerable<User>> GetUsersAsync()
        {
            return JsonConvert.DeserializeObject<IEnumerable<User>>(await _client.GetStringAsync($"users"));
        }
    }
}
