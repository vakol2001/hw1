﻿using System;
using System.Collections.Generic;

namespace ProjectManager.BL.Entities
{
    public record Team
    {
        public int Id { get; init; }
        public string Name { get; init; }
        public DateTime CreatedAt { get; init; }

        public IEnumerable<User> Participants { get; init; }
        public IEnumerable<Project> Projects { get; init; }

    }
}
