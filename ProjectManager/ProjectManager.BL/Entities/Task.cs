﻿using System;

namespace ProjectManager.BL.Entities
{
    public record Task
    {
        public int Id { get; init; }

        public int ProjectId { get; init; }
        public Project Project { get; init; }

        public int PerformerId { get; init; }
        public User Performer { get; init; }

        public string Name { get; init; }
        public string Description { get; init; }
        public int State { get; init; }
        public DateTime CreatedAt { get; init; }
        public DateTime? FinishedAt { get; init; }
    }
}
