﻿namespace ProjectManager.BL.Models
{
    public record TaskModel
    {
        public int Id { get; init; }
        public string Name { get; init; }
    }
}
