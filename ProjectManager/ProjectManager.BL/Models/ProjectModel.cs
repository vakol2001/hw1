﻿using ProjectManager.BL.Entities;

namespace ProjectManager.BL.Models
{
    public record ProjectModel
    {
        public Project Project { get; init; }
        public Task LongestTaskByDescription { get; init; }
        public Task ShortestTaskByName { get; init; }
        public int ParticipantsAmount { get; init; }
    }
}
